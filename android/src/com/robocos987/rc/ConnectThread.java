package com.robocos987.rc;

import android.bluetooth.BluetoothSocket;
import android.os.Build;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

public class ConnectThread extends Thread {
    BluetoothModule module;

    public ConnectThread(BluetoothModule module) {
            this.module = module;
        }
    public void run() {
        try{
            Class<?> clas = module.server.getClass();
            Class<?>[] params = new Class<?>[]{Integer.TYPE};
            Method m = clas.getMethod("createInsecureRfcommSocket", params);
            Object[] methodParams = new Object[]{Integer.valueOf(1)};
            module.socket = (BluetoothSocket) m.invoke(module.server, methodParams);
            module.socket.connect();
        }catch(Exception e) {
            e.printStackTrace();
            module.connected = false;
        }

        module.connected = true;
    }

        private void connect() throws Exception
        {
            byte[] packet = new byte[16];
            for(int i = 0; i < packet.length; i++)
                if(i % 2 == 0) {
                    packet[i] = 1;
                }else
                    packet[i] = 0;
            module.socket.getOutputStream().write(packet);
        }
}