package com.robocos987.rc;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static android.content.ContentValues.TAG;

/**
 * Created by walee on 3/9/2017.
 */

public class BluetoothModule implements BluetoothInterface {

    BluetoothAdapter adapter;
    AndroidLauncher launcher;

    BluetoothSocket socket;


    int REQUEST_ENABLE_BT = 1;

    DiscoveryReceiver receiver;

    String address, serverName;
    BluetoothDevice server;

    boolean bonded, searching, connected;

    final String uuid = "00001101-0000-1000-8000-00805F9B34FB";

    List<String> declinedServers = new ArrayList<String>();

    public BluetoothModule(AndroidLauncher launcher) {
        this.launcher = launcher;
        adapter = BluetoothAdapter.getDefaultAdapter();
        receiver = new DiscoveryReceiver(this);
    }

    public void start() {
        if (adapter != null) {
            // Quick permission check
            checkBTPerms();

            if (!adapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                launcher.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                search();
            }
        }
    }

    void checkBTPerms()
    {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            int permissionCheck = launcher.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += launcher.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                launcher.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        }else{
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }


    @Override
    public boolean bonded() {
        return bonded;
    }

    @Override
    public boolean connected() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return socket != null && socket.isConnected();
        }
        return false;
    }

    public void search() {
        searching = true;
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        launcher.registerReceiver(receiver, filter);


        if (adapter.isDiscovering())
            adapter.cancelDiscovery();
        adapter.startDiscovery();
    }

    public boolean searching() {
        return searching;
    }

    @Override
    public String address() {
        return address;
    }


    @Override
    public String serverName() {
        return serverName;
    }

    public void decline() {
        if (server != null && server.getAddress() != null)
            declinedServers.add(server.getAddress());
        server = null;
        search();
    }


    public void endSearch(BluetoothDevice device) {
        this.server = adapter.getRemoteDevice(device.getAddress());
        address = device.getAddress();
        serverName = device.getName();
        searching = false;
        adapter.cancelDiscovery();
    }

    public void startConnection() {
        Thread connectionThread = new ConnectThread(this);
        connectionThread.run();
    }

    public void sendData(byte data) throws Exception
    {
        System.out.println("Sending byte: " + data);
        socket.getOutputStream().write(data);
    }


}

