package com.robocos987.rc;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DiscoveryReceiver extends BroadcastReceiver {

    BluetoothModule module;
    public DiscoveryReceiver(BluetoothModule module) {
        this.module = module;
    }

    public DiscoveryReceiver() {

    }
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (BluetoothDevice.ACTION_FOUND.contains(action) && module.server == null) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (device != null) {
                if (device.getName() != null && device.getAddress() != null) {
                    if (!module.declinedServers.contains(device.getAddress())) {
                        System.out.println("RC Client: Found device: " + device.getName());
                        module.endSearch(device);
                    }
                }
            }
        }
    }
}
