package com.robocos987.rc;

/**
 * Created by walee on 3/9/2017.
 */

public interface BluetoothInterface {
    void start();
    void decline();
    void startConnection();

    boolean bonded();
    boolean connected();
    boolean searching();

    String address();
    String serverName();

    void sendData(byte data) throws Exception;
}
