package com.robocos987.rc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


/**
 * Created by walee on 3/15/2017.
 */

public class GUI {

    private RCMain main;
    private SpriteBatch batch;
    private BitmapFont font;

    private enum State{SEARCHING, CHOOSE, CONNECTING, PLAY}

    public State currentState = State.SEARCHING;

    Stage stage;

    ImageButton connect, decline;
    ImageButton up, down, left, right, stop;

    public GUI(RCMain main) {
        this.main = main;
        this.font = main.font;
        this.batch = new SpriteBatch();
        stage = new Stage();

        loadOptionButtons();
    }


    public void update(float delta) {
        switch(currentState) {
            case SEARCHING:
                if(!main.bluetooth.searching()) {
                    currentState = State.CHOOSE;
                }
                break;
            case CHOOSE:
                if(main.bluetooth.searching())
                    currentState = State.SEARCHING;
                if(main.bluetooth.connected()) {
                    loadDirectionButtons();
                    currentState = State.PLAY;
                }
                stage.act(delta);
                break;
            case CONNECTING:
                if(main.bluetooth.connected()) {
                    loadDirectionButtons();
                    currentState = State.PLAY;
                }
                break;
            case PLAY:
                if(up == null)
                    loadOptionButtons();
                else
                    stage.act(delta);
                break;
        }

    }


    public void loadOptionButtons() {
        ImageButton.ImageButtonStyle connectStyle = new ImageButton.ImageButtonStyle();
        connectStyle.imageUp = main.buttonSheet.getDrawable("connect");
        connect = new ImageButton(connectStyle);

        ImageButton.ImageButtonStyle declineStyle = new ImageButton.ImageButtonStyle();
        declineStyle.imageUp = main.buttonSheet.getDrawable("decline");
        decline = new ImageButton(declineStyle);

        connect.getImage().scaleBy(5);
        decline.getImage().scaleBy(5);
        connect.setPosition(Gdx.graphics.getWidth() / 8f, Gdx.graphics.getHeight() / 4f);
        decline.setPosition(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 4f);

        connect.addListener(new ClickListener(){

            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                System.out.println("Connect clicked");
                main.bluetooth.startConnection();
            }
        });

        decline.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                main.bluetooth.decline();
            }
        });

        stage.addActor(connect);
        stage.addActor(decline);

        Gdx.input.setInputProcessor(stage);
    }

    public void loadDirectionButtons() {
        System.out.println("Loading direction buttons..");
        ImageButton.ImageButtonStyle upStyle = new ImageButton.ImageButtonStyle();
        upStyle.imageUp = main.buttonSheet.getDrawable("up");
        up = new ImageButton(upStyle);
        up.setPosition(Gdx.graphics.getWidth() / 3f, Gdx.graphics.getHeight() / 1.8f);

        ImageButton.ImageButtonStyle downStyle = new ImageButton.ImageButtonStyle();
        downStyle.imageUp = main.buttonSheet.getDrawable("down");
        down = new ImageButton(downStyle);
        down.setPosition(Gdx.graphics.getWidth() / 3f, Gdx.graphics.getHeight() / 3.8f);

        ImageButton.ImageButtonStyle leftStyle = new ImageButton.ImageButtonStyle();
        leftStyle.imageUp = main.buttonSheet.getDrawable("left");
        left = new ImageButton(leftStyle);
        left.setPosition(Gdx.graphics.getWidth() / 10.5f, Gdx.graphics.getHeight() / 2.5f);

        ImageButton.ImageButtonStyle rightStyle = new ImageButton.ImageButtonStyle();
        rightStyle.imageUp = main.buttonSheet.getDrawable("right");
        right = new ImageButton(rightStyle);
        right.setPosition(Gdx.graphics.getWidth() / 1.8f, Gdx.graphics.getHeight() / 2.5f);

        ImageButton.ImageButtonStyle stopStyle = new ImageButton.ImageButtonStyle();
        stopStyle.imageUp = main.buttonSheet.getDrawable("stop");
        stop = new ImageButton(stopStyle);

        up.getImage().scaleBy(2);
        left.getImage().scaleBy(2);
        down.getImage().scaleBy(2);
        right.getImage().scaleBy(2);
        stop.getImage().scaleBy(2);

        stop.setPosition(Gdx.graphics.getWidth() / 1.4f, Gdx.graphics.getHeight() / 12f);


        up.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                try {
                    main.bluetooth.sendData((byte) 0);
                }catch(Exception e){e.printStackTrace();};
            }
        });

        down.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                try {
                    main.bluetooth.sendData((byte) 1);
                }catch(Exception e){e.printStackTrace();};
            }
        });

        left.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                try {
                    main.bluetooth.sendData((byte) 2);
                }catch(Exception e){e.printStackTrace();};
            }
        });

        right.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                try {
                    main.bluetooth.sendData((byte) 3);
                }catch(Exception e){e.printStackTrace();};
            }
        });

        stop.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                try {
                    main.bluetooth.sendData((byte) 4);
                }catch(Exception e){e.printStackTrace();};
            }
        });
        stage.addActor(up);
        stage.addActor(down);
        stage.addActor(left);
        stage.addActor(right);
        stage.addActor(stop);
    }

    public void render() {
        batch.begin();
        switch(currentState) {
            case SEARCHING:
                drawText("Searching...");
                break;
            case CHOOSE:
                drawText("Found device: " + main.bluetooth.serverName(), Gdx.graphics.getWidth() / 16f, Gdx.graphics.getHeight() / 2f);
                connect.draw(batch, 1.0f);
                decline.draw(batch, 1.0f);
                break;
            case PLAY:
                if (up != null) {
                    up.draw(batch, 1.0f);
                    down.draw(batch, 1.0f);

                    left.draw(batch, 1.0f);
                    right.draw(batch, 1.0f);
                    stop.draw(batch, 1.0f);
                }
                break;
            default:
                currentState = State.SEARCHING;
                break;
        }
        batch.end();
    }

    private void drawText(String text, float xPos, float yPos) {
        font.draw(batch, text, xPos, yPos);
    }

    private void drawText(String text) {
        drawText(text, Gdx.graphics.getWidth() / 4f, Gdx.graphics.getHeight() / 2f);
    }


    public void dispose() {
        batch.dispose();
    }
}
