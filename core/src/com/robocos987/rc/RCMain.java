package com.robocos987.rc;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class RCMain extends ApplicationAdapter {

	BitmapFont font;
	GUI gui;

	Skin buttonSheet;

	BluetoothInterface bluetooth;

	public RCMain(BluetoothInterface bluetoothInterface) {
		this.bluetooth = bluetoothInterface;
		bluetooth.start();
	}

	@Override
	public void create () {
		font = new BitmapFont(Gdx.files.internal("font.fnt"));
		font.setColor(Color.BLACK);
		font.getData().scale(2.5f);

		buttonSheet = new Skin(new TextureAtlas(Gdx.files.internal("buttons.pack")));
		gui = new GUI(this);
	}

	@Override
	public void render () {
//		Gdx.gl.glClearColor(0.55f, 0.5f, 0.5f, 0.3f);
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gui.update(Gdx.graphics.getDeltaTime());
		gui.render();
	}

	
	@Override
	public void dispose () {
		font.dispose();
		gui.dispose();
	}

}
