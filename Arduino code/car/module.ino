#include <Wire.h>
#include <Servo.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

int tx = 0, rx = 1;
Servo turning;

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *myMotor = AFMS.getMotor(1);

bool straight = true;

void setup() {
 Serial.begin(9600);
AFMS.begin();
pinMode(tx, OUTPUT);
pinMode(rx, INPUT);
turning.attach(9);
}

void loop() {
  while(Serial.available() > 0)
  {
    int inSerial = Serial.read();
    switch(inSerial)
    {
      //stop command
      case -1:
      myMotor->setSpeed(0);
      myMotor->run(FORWARD);
      turning.writeMicroseconds(1500);     
      break;
      
      //forward
      case 0:    
      myMotor->setSpeed(100); 
      myMotor->run(BACKWARD);
      break;

      //backward
      case 1:
      myMotor->setSpeed(100);
      myMotor->run(FORWARD);
      break;

      //left turn
      case 2:
      if(straight)
      {
        turning.writeMicroseconds(1300);
        straight = false;
      }
      else
      {
        turning.writeMicroseconds(1500);
        straight = true;
      }     
      break;

      //right turn
      case 3:
       if(straight)
      {
        turning.writeMicroseconds(1700);
        straight = false;
      }
      else
      {
        turning.writeMicroseconds(1500);
        straight = true;
      }     
      break;
    }
  }
}
